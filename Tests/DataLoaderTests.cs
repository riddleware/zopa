﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zopa;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class DataLoaderTests
    {
        [TestMethod]
        public void ValidFile()
        {
            List<Lender> lenders = new List<Lender>();
            DataLoader.Load("markettest.csv", lenders);

            Assert.AreEqual(lenders.Count, 7);
        }
    }
}
