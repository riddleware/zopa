﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zopa;
using System.Diagnostics;

namespace Tests
{
    [TestClass]
    public class QuoteProcessorTests
    {
        [TestMethod]
        public void CalculateMonthlyPaymentInvalidZeroTest()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv",0,36);

            Assert.AreEqual(processor.CalculateMonthlyPayment(0, 0),0);
        }

        [TestMethod]
        public void CalculateMonthlyPaymentInvalidNegativeTest()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 0, 36);

            Assert.AreEqual(processor.CalculateMonthlyPayment(-100, 10), 0);
        }

        [TestMethod]
        public void CalculateMonthlyPaymentValidTest()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 0, 36);
            
            decimal payment = processor.CalculateMonthlyPayment(1000, 10);
            
            Assert.AreEqual(payment, 833.33M);
        }

        [TestMethod]
        public void CalculateWeightedInterestTestSimple()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 1000, 36);

            decimal rate = 0.01M;

            while (rate <= 1)
            {
                decimal value = processor.CalculateWeightedInterest(1000M, rate);

                Assert.AreEqual(value, rate * 100);
                
                rate += 0.01M;
            }
        }

        [TestMethod]
        public void CalculateWeightedInterestTestDivisions()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 1000, 36);

            decimal divider = 1M;
            
            while (divider <= 100)
            {
                decimal rate = 0.01M;

                while (rate <= 1)
                {
                    decimal value = Math.Round(processor.CalculateWeightedInterest(1000M / divider, rate),5);

                    Assert.AreEqual(value, Math.Round((rate * 100) / divider, 5));

                    rate += 0.01M;
                }

                divider++;
            }
        }

        [TestMethod]
        public void ProcessQuoteInvalidValueTest()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 999, 36);

            //should throw exception
            try
            {
                Quote bestQuote = processor.ProcessQuote();
                Assert.Fail();
            }
            catch (Exception Ex)
            {
                Assert.IsNotNull(Ex);
            }

            processor = new QuoteProcessor("markettest.csv", 120, 36);

            //should throw exception
            try
            {
                Quote bestQuote = processor.ProcessQuote();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }

            processor = new QuoteProcessor("markettest.csv", 15001, 36);

            //should throw exception
            try
            {
                Quote bestQuote = processor.ProcessQuote();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }

            //invalid term, restricting to 36 for time being
            processor = new QuoteProcessor("markettest.csv", 1000, 24);

            //should throw exception
            try
            {
                Quote bestQuote = processor.ProcessQuote();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        //Need to create more tests comparing to a range of known values, taken from  an external dataset.
        [TestMethod]
        public void ProcessQuoteTest()
        {
            QuoteProcessor processor = new QuoteProcessor("markettest.csv", 1000, 36);

            Quote bestQuote = processor.ProcessQuote();

            Assert.AreEqual(bestQuote.Rate, 7.004M);
            Assert.AreEqual(bestQuote.MonthlyRepayment, 30.88M);
            Assert.AreEqual(bestQuote.TotalRepayment, bestQuote.MonthlyRepayment * bestQuote.TermDurationMonths);            
        }
    }
}
