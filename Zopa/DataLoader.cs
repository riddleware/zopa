﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Zopa
{
    public class DataLoader
    {
        public static void Load(string pFilename, List<Lender> pLenders)
        {           
            try
            {
                //Assume small dataset, read into memory for simplicity
                string[] lines = File.ReadAllLines(pFilename);

                //Skip header
                for (int i = 1; i < lines.Length; i++)
                {
                    var cols = lines[i].Split(',');
                    pLenders.Add(new Lender()
                    {
                        Name = cols[0],
                        Rate = decimal.Parse(cols[1]),
                        Available = decimal.Parse(cols[2]),
                    });
                }                
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading data file", ex);
            }            
        }
    }
}
