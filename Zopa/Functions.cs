﻿namespace Zopa
{
    public class Functions
    {
        public static bool ValidateInput(int pLoanTerm, int pRequestedAmount)
        {
            return !(pLoanTerm != 36
                || pRequestedAmount % 100 != 0
                || pRequestedAmount < 1000
                || pRequestedAmount > 15000);
        }
    }
}
