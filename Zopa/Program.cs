﻿using System;

namespace Zopa
{
    internal class Program
    {
        private static void Main(string[] args)
        {   
            //keeping error handling simple
            try
            {
                if (args.Length < 2)
                {
                    Console.WriteLine("Invalid number of arguments supplied");
                }
                else
                {
                    const int loanTerm = 36; //hard coded
                                             // Assuming dealing with whole numbers only for now, as per req.

                    if (int.TryParse(args[1], out int requestedAmount)
                        || !Functions.ValidateInput(loanTerm, requestedAmount))
                    {
                        QuoteProcessor processor = new QuoteProcessor(args[0], requestedAmount, loanTerm);
                        Quote bestQuote = processor.ProcessQuote();

                        if (bestQuote.UnfulfilledAmout > 0)
                        {
                            Console.WriteLine(string.Format("Sorry, it is not possible to provide a quote at this time.", requestedAmount));
                        }
                        else
                        {
                            Console.WriteLine($"Requested amount: £{requestedAmount}");
                            Console.WriteLine($"Rate: {Math.Round(bestQuote.Rate, 1)}%");
                            Console.WriteLine($"Monthly repayment: £{bestQuote.MonthlyRepayment}");
                            Console.WriteLine($"Total repayment: £{bestQuote.TotalRepayment}");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid amount supplied");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey(); 
        }
    }
}
