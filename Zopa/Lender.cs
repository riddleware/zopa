﻿
namespace Zopa
{
    public class Lender
    {
        public string Name { get; set; }

        public decimal Rate { get; set; }

        public decimal Available { get; set; }
    }
}
