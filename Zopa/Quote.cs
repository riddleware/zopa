﻿
namespace Zopa
{
    public class Quote
    {
        public decimal RequestedAmount { get; set; }
        public decimal Rate { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public decimal TotalRepayment { get; set; }
        public decimal TermDurationMonths { get; set; }

        public decimal UnfulfilledAmout { get; set; }
    }
}
