﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zopa
{
    public class QuoteProcessor
    {
        public int RequestedAmount { get; set; }
        List<Lender> _lenders = new List<Lender>();

        private Quote Result { get; }
        private int LoanTerm { get; }


        public QuoteProcessor(string pFilename, int pRequestedAmount, int pLoanTerm)
        {        
            //Initialise variables
            RequestedAmount = pRequestedAmount;
            LoanTerm = pLoanTerm;

            Result = new Quote
            {
                RequestedAmount = RequestedAmount,
                UnfulfilledAmout = RequestedAmount,
                TermDurationMonths = LoanTerm
            };

            //Load data, will throw exception back on failure
            DataLoader.Load(pFilename, _lenders);            
        }

        public Quote ProcessQuote()
        {
            if (!Functions.ValidateInput(LoanTerm, RequestedAmount))
                throw (new Exception("Invalid values entered")); 

            //Order by lowest rate first, then smallest amount available.
            _lenders = _lenders.OrderBy(l => l.Rate).ThenBy(l => l.Available).ToList();                        

            //Borrow as much as we need / can from each lender in turn.
            foreach (Lender iLender in _lenders)
            {
                //How much do we need to / can we borrow from lender.
                decimal amountToBorrowFromLender = Result.UnfulfilledAmout > iLender.Available ?
                                                                iLender.Available
                                                                :
                                                                Result.UnfulfilledAmout;

                //Monthly repayment for this 'sub' loan
                Result.MonthlyRepayment += Math.Round(CalculateMonthlyPayment(amountToBorrowFromLender, iLender.Rate), 2);

                //Amount still needed
                Result.UnfulfilledAmout -= amountToBorrowFromLender;

                //Figure out 'weighted' interest for this loan (for totals for display only)
                Result.Rate += CalculateWeightedInterest(amountToBorrowFromLender, iLender.Rate);

                //Loan fulfilled
                if (Result.UnfulfilledAmout == 0)
                    break;
            }
                        
            //Total repayment
            Result.TotalRepayment = Math.Round(Result.MonthlyRepayment * LoanTerm, 2);            

            return Result;
        }

        public decimal CalculateMonthlyPayment(decimal pAmount, decimal pRate)
        {
            if (pAmount <= 0 || pRate <= 0)
                return 0;

            decimal monthlyRate = pRate / 12;
            decimal monthlyRateExp = (decimal)Math.Pow((double)monthlyRate + 1.0, (double)LoanTerm);

            return Math.Round((pAmount * monthlyRate * monthlyRateExp) / (monthlyRateExp - 1), 2);
        }

        public decimal CalculateWeightedInterest(decimal pAmountToBorrowFromLender, decimal pRate)
        {
            if (pAmountToBorrowFromLender <= 0 || pRate <= 0)
                return 0;

            decimal loanPercent = (pAmountToBorrowFromLender / RequestedAmount) * 100;
            return (loanPercent * pRate);
        }        
    }
}
